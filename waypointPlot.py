print "EZEZEZEZ"

import glob
import os
import sys

try:
    sys.path.append(glob.glob('**/*%d.%d-%s.egg' % (
        sys.version_info.major,
        sys.version_info.minor,
        'win-amd64' if os.name == 'nt' else 'linux-x86_64'))[0])
except IndexError:
    pass

import carla
import argparse
import random
import time
import numpy as np
import matplotlib.pyplot as plt
from funcDef import *
import math

actor_list=[]
def d():
    for i in actor_list:
        i.destroy()
    print '  all destoryed!!'
    quit()

try:
    client = carla.Client('localhost',2000) # taking TCP ports 2000 and 2001
    client.set_timeout(5.0) #sec
    world = client.get_world()
    map = world.get_map()
    bpLib = world.get_blueprint_library()
    sensors = bpLib.filter('sensor.*')
    vehicles = bpLib.filter('vehicle.*')
    bikes = [x for x in vehicles if int(x.get_attribute('number_of_wheels')) == 2]
    cars = [x for x in vehicles if int(x.get_attribute('number_of_wheels')) == 4]

    waypoint_tuple_list = map.get_topology()
    x,y,z=[0],[0],[0]
    yaw,pch,rol=[0],[0],[0]
    u,v=[0],[0]

    for wp_tuple in waypoint_tuple_list:
        for i in range(2):
            x.append(wp_tuple[i].transform.location.x)
            y.append(wp_tuple[i].transform.location.y)
            z.append(wp_tuple[i].transform.location.z)
            yaw.append(wp_tuple[i].transform.rotation.yaw)
            pch.append(wp_tuple[i].transform.rotation.pitch)
            rol.append(wp_tuple[i].transform.rotation.roll)
            u.append(np.cos(yaw[-1]))
            v.append(np.sin(yaw[-1]))
        plt.plot(y[-3:-1],x[-3:-1],'k-')

    if 1:
        plt.quiver(y,x,v,u,color=['r','b'])#, scale=21)
        plt.ylabel('X(m)')
        plt.xlabel('Y(m)')
        plt.title("WayPoint -Topology Plot")
        plt.show()




finally:
    print('\ndone.')

# Eagle - Dapeng Zhao
# dapengz@andrew or tim.eagle.zhao@gmail.com
# 29 Jan 2019



import glob
import os
import sys

try:
    sys.path.append(glob.glob('**/*%d.%d-%s.egg' % (
        sys.version_info.major,
        sys.version_info.minor,
        'win-amd64' if os.name == 'nt' else 'linux-x86_64'))[0])
except IndexError:
    pass

import carla
import argparse
import random
import time
import numpy as np
import matplotlib.pyplot as plt




actor_list=[]
def d():
    for i in actor_list:
        i.destroy()
    print '  all destoryed!!'
    quit()

try:
    client = carla.Client('localhost',2000) # taking TCP ports 2000 and 2001
    client.set_timeout(5.0) #sec
    world = client.get_world()
    map = world.get_map()
    bpLib = world.get_blueprint_library()
    sensors = bpLib.filter('sensor.*')
    vehicles = bpLib.filter('vehicle.*')
    bikes = [x for x in vehicles if int(x.get_attribute('number_of_wheels')) == 2]
    cars = [x for x in vehicles if int(x.get_attribute('number_of_wheels')) == 4]


    birthPoint = carla.Transform(carla.Location(x=200, y=-1, z=1), carla.Rotation(yaw=180))
    car0 = world.spawn_actor(cars[0],birthPoint)
    actor_list.append(car0)
    car0.set_simulate_physics(True)
    car0.set_autopilot(False)
    wp = map.get_waypoint(car0.get_location())
    print 'The birthPoint is ',wp
    car0.set_transform(wp.transform)

    trans=[carla.Transform(carla.Location(x=200, y=-2, z=0), carla.Rotation(yaw=180)),\
            carla.Transform(carla.Location(x=197, y=-2, z=0), carla.Rotation(yaw=180)),\
            carla.Transform(carla.Location(x=194, y=-2, z=0), carla.Rotation(yaw=180)),\
            carla.Transform(carla.Location(x=191, y=-2.7, z=0), carla.Rotation(yaw=187)),\
            carla.Transform(carla.Location(x=188, y=-4, z=0), carla.Rotation(yaw=195)),\
            carla.Transform(carla.Location(x=185, y=-5.3, z=0), carla.Rotation(yaw=187)),\
            carla.Transform(carla.Location(x=182, y=-6, z=0), carla.Rotation(yaw=180)),\
            carla.Transform(carla.Location(x=179, y=-6, z=0), carla.Rotation(yaw=180)),\
            carla.Transform(carla.Location(x=176, y=-6, z=0), carla.Rotation(yaw=180)),\
            ]

    acc=[]
    vel=[]
    for i in trans:
        car0.set_transform(i)
        t0=time.time()
        while time.time()-t0<0.5:
            acc.append(car0.get_acceleration())
            vel.append(car0.get_velocity())

    if 0:
        leng=len(acc)
        tx=range(leng)
        acc_x,vel_x = np.zeros(leng),np.zeros(leng)
        for i in tx:
            acc_x[i],vel_x[i] = acc[i].x,vel[i].x

        plt.plot(tx,acc_x,'.-',label='Acceleration')
        plt.plot(tx,vel_x,'.-',label='Velocity')
        #plt.axis([0, 100, -1, 1])
        plt.ylabel('Acc&Vel')
        plt.xlabel('Index (linear to time)')
        plt.legend()
        plt.show()

    '''
    while 1:
        wp=random.choice(wp.next(2))
        car0.set_transform(wp.transform)
        #car0.set_location(wp.transform.location)
        print (car0.get_acceleration())
        print (car0.get_velocity())
        ch=raw_input()
        if ch=='q':
            break
    '''



#    for i in bikes:
#    cnt=0
#        cnt+=1
#        transform = carla.Transform(carla.Location(x=0, y=10, z=0+cnt*5), carla.Rotation(yaw=180))
#        actor=world.spawn_actor(i, transform)
#        actor_list.append(actor)



finally:

    print('\ndone.')

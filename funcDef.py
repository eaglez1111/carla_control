
import glob
import os
import sys
import math

sys.path.append(glob.glob('**/*%d.%d-%s.egg' % (
    sys.version_info.major,
    sys.version_info.minor,
    'win-amd64' if os.name == 'nt' else 'linux-x86_64'))[0])

import numpy as np
import carla

class PIDController:
    def __init__(self,Kp,Ki=0,Kd=0,staticErr=0):
        self.Kp = Kp
        self.Ki = Ki
        self.Kd = Kd
        self.staticErr=staticErr

        self.I_start=0
        self.I=0
        self.errLast=0

    def resetI(self):
        self.I_start=0
        self.I=0

    def output(self,err,Kp=0,Ki=0,Kd=0):   # err = reference - measurement
        if (Kp!=0):
            self.Kp=Kp
        if (Ki!=0):
            self.Ki=Ki
        if (Kd!=0):
            self.Kd=Kd

        if np.abs(err)<np.abs(self.staticErr):
            self.I_start=1

        P = self.Kp*err
        self.I += self.Ki*err*self.I_start # - self.Ki*errToBeDeducted
        D = self.Kd*(err-self.errLast)
        self.errLast = err

        if self.staticErr==2:
            print (P+self.I+D),err,(err-self.errLast),self.Kd,D
        return (P+self.I+D)

a=4
'''Calculate yaw error '''
def calcYawErr(car,x1,y1):
    tr = car.get_transform()
    x0,y0 = tr.location.x,tr.location.y
    yaw = tr.rotation.yaw
    yawR=np.arctan( (y1-y0)/(x1-x0) ) *180/np.pi
    #print yawR,yaw,(yawR-yaw+90)%180-90
    return (yawR-yaw+90)%180-90

'''Calculate vihecle speed(km/h) '''
def calcSpeed(c):
    v = c.get_velocity()
    return (3.6 * math.sqrt(v.x ** 2 + v.y ** 2 + v.z ** 2))

class CarController:
    def __init__(self,car,accelC,steerC,throtBias=0.625):
        self.car = car
        self.steerC = steerC
        self.accelC = accelC
        self.throtBias = throtBias
        loc=car.get_location()
        self.x1,self.y1,self.v1 = loc.x,loc.y,calcSpeed(car)
        self.x0,self.y0,self.v0,self.yawErr = [],[],[],[]
        self.throtVal,self.brakeVal,self.steerVal = [],[],[]

    def setTarget(self,x1,y1,v1):
        if(v1!=self.v1):
            self.accelC.resetI()
        self.x1,self.y1,self.v1 = x1,y1,v1

    def calcThrot(self):
        err = ( self.v1 - calcSpeed(self.car) )
        err = err/abs(err)*(err**1)

        throtVal = self.throtBias + self.accelC.output(err)
        throtVal = max(0, min(1,throtVal))
        return throtVal

    def calcSteer(self):
        err = calcYawErr(self.car,self.x1,self.y1)
        steerVal = self.steerC.output( err )
        steerVal = max(-1, min(1,steerVal))
        self.yawErr.append(err)
        return steerVal

    def calcBrake(self):
        return 0

    def updateControl(self):
        t,s,b = self.calcThrot(),self.calcSteer(),self.calcBrake()
        self.car.apply_control( carla.VehicleControl(throttle=t,steer=s,brake=b) )

        self.saveState()
        self.throtVal.append(t)
        self.brakeVal.append(s)
        self.steerVal.append(b)

    def saveState(self):
        tr=self.car.get_transform()
        self.x0.append(tr.location.x)
        self.y0.append(tr.location.y)
        self.v0.append(calcSpeed(self.car))

def findClosestPoint(path,x0,y0):
    dist2 = np.array(np.zeros(len(path[0])))
    for i in range(len(path[0])):
        dist2[i] = (path[0,i]-x0)**2 + (path[1,i]-y0)**2
    #print dist2
    return np.argmin(dist2)

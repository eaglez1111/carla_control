# Eagle - Dapeng Zhao
# dapengz@andrew or tim.eagle.zhao@gmail.com
# 15 Feb 2019
print "EZEZEZEZ"

import glob
import os
import sys

try:
    sys.path.append(glob.glob('**/*%d.%d-%s.egg' % (
        sys.version_info.major,
        sys.version_info.minor,
        'win-amd64' if os.name == 'nt' else 'linux-x86_64'))[0])
except IndexError:
    pass

import carla
import argparse
import random
import time
import numpy as np
import matplotlib.pyplot as plt
from funcDef import *
import math

actor_list=[]
def d():
    for i in actor_list:
        i.destroy()
    print '  all destoryed!!'
    quit()

try:
    client = carla.Client('localhost',2000) # taking TCP ports 2000 and 2001
    client.set_timeout(5.0) #sec
    world = client.get_world()
    map = world.get_map()
    bpLib = world.get_blueprint_library()
    sensors = bpLib.filter('sensor.*')
    vehicles = bpLib.filter('vehicle.*')
    bikes = [x for x in vehicles if int(x.get_attribute('number_of_wheels')) == 2]
    cars = [x for x in vehicles if int(x.get_attribute('number_of_wheels')) == 4]


    #wp = map.get_waypoint(car0.get_location())
    #car0.set_transform(wp.transform)
    #print 'The birthPoint is ',wp
    #car0.set_transform(wp.carla.Transform(carla.Location(x=-85, y=-20, z=1), carla.Rotation(yaw=90)))

    from path import path

    birthPoint = carla.Transform(carla.Location(x=path[0,0]-10, y=path[1,0], z=0.2), carla.Rotation(yaw=0))
    car0 = world.spawn_actor(cars[0],birthPoint)
    #time.sleep(0.5)
    actor_list.append(car0)
    car0.set_simulate_physics(True)
    car0.set_autopilot(False)

    carC = CarController( car0,\
                        PIDController(0.009,0.0006,0.01,2),\
                        PIDController(0.015,0,0.0005,0),\
                        0.625 )

    step_size=2
    vRef=[]
    while(1):
        loc=carC.car.get_location()
        path_id = findClosestPoint(path,loc.x,loc.y)+step_size
        #print path_id
        if path_id >= len(path[0]):
            break

        x1,y1,v1 = path[:,path_id]
        vRef.append(v1)
        carC.setTarget(x1,y1,v1)
        carC.updateControl()
        time.sleep(0.02)

    car0.apply_control(carla.VehicleControl(brake=1))

    if 1:
        plt.subplot(2, 1, 1)
        plt.plot(path[1,:],path[0,:],'.-',label='Reference')
        plt.plot(np.array(carC.y0),np.array(carC.x0),'.-',label='Measurement')
        plt.ylabel('X(m) - Latitude')
        plt.xlabel('Y(m) - Longitude')
        plt.title("Path (Top View) : Reference & Measurement")
        #plt.legend()
        #plt.show()

        plt.subplot(2, 1, 2)
        plt.plot(vRef,'.-',label='Reference')
        plt.plot(np.array(carC.v0),'.-',label='Measurement')
        plt.ylabel('Speed(km/h)')
        plt.xlabel('Y(m) - Longitude')
        plt.title("Speed Plot : Reference & Measurement")
        #plt.legend()
        plt.show()
            #plt.subplot(3, 1, 1)
            #plt.plot(carC.y0,carC.yawErr,'.-',label='measurement')
        #plt.plot(y0,v0,'.-',label='Throt')
        #plt.ylabel('Vel')
        #plt.xlabel('Index (linear to time)')
        #plt.legend()
        plt.show()




finally:
    print('\ndone.')

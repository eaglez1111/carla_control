import numpy as np
import matplotlib.pyplot as plt


y0,x0 = 10,-80
y = np.array(range(100+1),dtype=np.float)+y0
x,v = 0*y,0*y
x[10:51] = x0 - 5*np.cos( (y[10:51]-y[10])*np.pi/40 )
v[10:51] = 57 + 3*np.cos( (y[10:51]-y[10])*np.pi/20 )
x[:10],x[51:] = [x[10]]*10,[x[50]]*50
v[:10],v[51:] = [v[10]]*10,[v[50]]*50

'''Town04: Right turn'''

x0,y0,r = 246.75,-237.75,8.25
tht=np.pi/180*(90.0/13*np.array(range(13+1)))
x = x0+r*np.sin(tht)
y = y0-r*np.cos(tht)
v = 25 + 0.5*np.cos( tht*4 )
x_pre = x0+np.array(range(-10,0))
y_post= y0+np.array(range(1,11))
x=np.append(np.append(x_pre,x),[x[-1]]*10)
y=np.append(np.append([y[0]]*10,y),y_post)
v=np.append(np.append([v[0]]*10,v),[v[-1]]*10)




'''Town04: Left turn'''
x0,y0,r = 246.75,-258,12.0
tht=np.pi/180*np.array(range(19))*5
x = x0+r*np.sin(tht)
y = y0+r*np.cos(tht)
v = 23 + 1*np.cos( tht*4 )
x_pre = x0+np.array(range(-10,0))
y_post= y0+np.array(range(-1,-11,-1))
x=np.append(np.append(x_pre,x),[x[-1]]*10)
y=np.append(np.append([y[0]]*10,y),y_post)
v=np.append(np.append([v[0]]*10,v),[v[-1]]*10)


if 0:
    plt.plot(y,x,'.-')
    plt.ylabel('X(m)')
    plt.xlabel('Y(m)')
    plt.show()

    plt.plot(range(len(v)),v,'.-')
    plt.ylabel('Vel(km/h)')
    plt.xlabel('Y(m)')
    plt.show()

path=np.array([x,y,v])
